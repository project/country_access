
-- SUMMARY --

Country access module provides the ability to add a list of countries that 
you wish to allow or deny access your site.

-- REQUIREMENTS --

Actually working with Smart IP and IP-based determination of Country

-- INSTALLATION --

Enable it from module page

-- CONFIGURATION --

* Go to admin/config/people/country_access
* Add a list of countries
* Customize your message
* Select the action to perform on the contries listed above
* Choose a page to redirect to
