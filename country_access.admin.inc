<?php

/**
 * @file
 * The country access.
 */

/**
 * Implements hook_form().
 */
function country_access_admin_form($form, &$form_state) {
  $supported_modules_option = country_access_active();
  $form['country_access_ip_module'] = array(
    '#type' => 'radios',
    '#required' => TRUE,
    '#title' => t('Which module would like to use?'),
    '#options' => $supported_modules_option,
    '#default_value' => variable_get('country_access_ip_module', 0),
    '#description' => t('Choose the module for IP to country detection') . '<br />' .
    t('supported modules') . ' ' . l(t('Smart IP'), 'https://www.drupal.org/project/smart_ip') . ' ' . t('or') . ' ' . l(t("IP-based Determination of a Visitor's Country"), "https://www.drupal.org/project/ip2country"),
  );
  $form['country_access_country'] = array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => t('Country code'),
    '#default_value' => variable_get('country_access_country', 'GB'),
    '#description' => t('Enter the country codes you wish to restrict separated by comma:') . '<br />' . t('GB,IT,BR') . '<br />' .
    t('Country code') . ' ' . l(t('list'), "http://userpage.chemie.fu-berlin.de/diverse/doc/ISO_3166.html") . t('. Column A2'),
  );
  $form['country_access_behaviour'] = array(
    '#type' => 'radios',
    '#required' => TRUE,
    '#title' => t('Select the action to perform on the countries listed above'),
    '#description' => '<b>' . t('Caution:') . '</b> <br />' . t('If you deny the above list all other countries will be allowed.') . '</br />' . t('If you allow the list above, all other countries will be refused'),
    '#options' => array(
      0 => t('Deny'),
      1 => t('Allow'),
    ),
    '#default_value' => variable_get('country_access_behaviour', 0),
  );
  $form['country_access_message'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Error message'),
    '#default_value' => variable_get('country_access_message', t('Country not allowed')),
    '#description' => t('Enter the error message you want the user to see if the country is not allowed.'),
  );
  $form['country_access_page'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Page callback'),
    '#default_value' => variable_get('country_access_page', t('country-not-allowed')),
    '#description' => t('Enter here your custom node page url') . '<br />node/123',
  );

  if (empty($supported_modules_option)) {
    variable_set('country_access_active', 0);
    drupal_set_message(t('You must install Smart IP or IP-based determination of Country'));
    foreach ($form as $key => $form_item) {
      $form[$key]['#disabled'] = TRUE;
    }
  }
  else {
    variable_set('country_access_active', 1);
  }

  $form['#validate'][] = 'country_access_admin_form_validate';

  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function country_access_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (empty($values['country_access_page'])) {
    $form_state['values']['country_access_page'] = 'country-not-allowed';
  }
  if (empty($form_state['values']['country_access_country'])) {
    form_set_error('country_access_country', t('Country list cannot be empty.'));
  }
}
